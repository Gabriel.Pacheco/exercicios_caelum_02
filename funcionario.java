// o Exercicio 07 já havia sido feito no Exercicio 06

public class funcionario{
	
	data dataEntrada = new data();
	
	private String nome;
	private double salario;
	private String RG;
	private String departamento;
	
	public void setNome(String umNome){
	
	      nome = umNome;
	      
	}

	public String getNome(){
	
	  return nome;
	
	}
	
	public void setDEntrada(int dia, int mes, int ano){
	
	      dataEntrada.setDia(dia);
	      dataEntrada.setMes(mes);
	      dataEntrada.setAno(ano);
	      
	}
	
	
	public void setSalario(double umSalario){
	
		salario = umSalario;
	
	}
	
	public double getSalario(){
	
	  return salario;
	
	}
	
	public void setRG(String umRG){
	
		RG = umRG;
	
	}
	
	public String getRG(){
	
	  return RG;
	
	}
	
	public void setDepartamento(String umDepartamento){
	
		departamento = umDepartamento;
	
	}
	
	public String getDepartamento(){
	
	  return departamento;
	
	}
	
	public double recebeAumento(double aumento){
	
	  return salario = aumento + salario;
	
	}
	
	public double calculaGanhoAnual(){
	
	  return (salario)*12;
	
	}
	
	public void mostra(){
	
	System.out.println("\nFuncionario inserido com sucesso");
	System.out.println("\nNome " + getNome());
	System.out.println("\nDepartamento " + getDepartamento());
	System.out.println("\nRG " + getRG());
	System.out.println("Data de entrada: " + dataEntrada.formatada());
	System.out.println("\nSalario " + recebeAumento(10));
	System.out.println("\nSalario Anual " + calculaGanhoAnual());
	
	}
	
}